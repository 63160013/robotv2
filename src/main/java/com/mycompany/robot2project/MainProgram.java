/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.robot2project;
import java.util.Scanner;
/**
 *
 * @author BankMMT
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner pm = new Scanner (System.in);
        TableMap map = new TableMap (10,10);
        Robot robot = new Robot(2,2,'x',map);
        Bomb bomb = new Bomb(5,5);
        map.setBomb(bomb);
        map.setRobot(robot);
        while(true){
            map.showMap();
            char direction = inputDirection(pm);
            if (direction == 'q'){
                System.out.println("Bye Bye!!");
                break;
            }
            robot.walk(direction);
        }
        map.showMap();
    }public static char inputDirection(Scanner pm){
        String str = pm.next();
        return str.charAt(0);
    }
}
