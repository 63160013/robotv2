/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.robot2project;

/**
 *
 * @author BankMMT
 */
public class TableMap {
    private int width;
    private int height;
    private Robot robot;
    private Bomb bomb;
    
    public TableMap(int width, int height){
        this.width = width;
        this.height = height;
    }
    public void setRobot(Robot robot){
        this.robot = robot;
    }
    public void setBomb (Bomb bomb){
        this.bomb = bomb;
    }
    public void showMap(){
        System.out.println("Map");
        for (int y=0 ; y < height ; y++){
            for (int x = 0 ; x < width ; x++){
                if (robot.isOn(x,y)){
                    System.out.print(robot.getSymbol());
                }else if (bomb.isOn(x,y)){
                    System.out.print(bomb.getSymbol());
                }else {
                    System.out.print("-");
                }
            }System.out.println("");
        }
    }
    public boolean inMap(int x, int y){
        return true;
    }
    public boolean isBomb(int x, int y){
        return bomb.isOn(x,y);
    }
}
